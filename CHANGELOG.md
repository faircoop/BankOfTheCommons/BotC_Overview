# Changelog
All notable changes to BotC wallet  will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Undeployed]
- Applying for BotC membership in OCP from wallet
- Displaying number of shares directly from OCP

## [0.9.5]
### Added
- BuyShares Method next to other wallet method for buying BotC shares directly from wallet

### Changed
- Better visibility of wallet actions (CSV Export) instead of three dot's
- Spark email is sending from wrong adress
- RecoveryPassword email template is now showing proper design
- RecoveryPassword screen is now using proper design
- Organization Token is now called Account Token

### Fixed
- Update of mail-configuration & removing of hacking switmailer libary

## [0.9.4]
### Changed 
- [FairPay Login](https://wallet.bankofthecommons.coop/login/fairpay) Improved loading time and update icon set  

### Fixed
- Wallet2Wallet transactions showing wrong Sender
- Sepa CashIn show wrong IBAN/Swift when checking transaction Details
- Export CSV of transactions doesn't show currency
- Login page is trying to load loader.white.gif wich doesnt exist

## [0.9.3-hotfix]
### Fixed
- Wallet2Wallet transfer 2FA checking and removed bypass of 2FA

## [0.9.3-legacy]
### Added
- From now on When doing cash-out via Spark cards, there are 3 more fields required: 
First Name
Last Name
Country of registration (Must be EEA country from list)

### Changed
- Company Account is renamed to Organization Account because it is more generally.
- Comapny Token is now Organization Token as `company` is deprecated.

### Fixed
- It was possible to do actions on disabled methods (cashin/cashout) as for example 
Ceativecoin or CRYPTOCAPITAL. From now on clicking on this disabled methods will
do nothing as expected.
- All mail templates use standard BotC design guildlines


## [0.9.2-legacy]
### Added
- This CHANGELOG file to hopefully serve as an evolving example of a
  standardized open source project CHANGELOG.