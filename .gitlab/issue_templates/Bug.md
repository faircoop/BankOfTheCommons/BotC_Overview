<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered "bug" label.

For Wallet Backend issue tracker:
- https://git.fairkom.net/faircoop/BankOfTheCommons/BotC_Overview/issues?label_name%5B%5D=bug

For Wallet Frontend issue tracker:
- https://git.fairkom.net/faircoop/BankOfTheCommons/wallet-frontend/issues?label_name%5B%5D=bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's tough to read otherwise.)

### Output of checks

(If you are reporting a bug on development env, write: This bug happens on DEV env)

### Service status

(Did you checked on https://status.bankofthecommons.coop is some service affected ?)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug
